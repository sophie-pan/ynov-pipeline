stages:
  - build
  - test
  - quality
  - package
  - security
  - deploy
  - acceptance
  - tdv
  - release

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

composer:install:
  stage: build
  image: composer:lts
  script:
    - cd src && composer install
  artifacts:
    paths:
      - src/vendor/
  rules:
    - if: $CI_COMMIT_TAG != null || $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"

phpunit:
  stage: test
  image: php:8.1
  script:
    - php src/vendor/bin/phpunit tests/
  rules:
    - if: $CI_COMMIT_TAG != null || $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"

sonarcloud-check:
  stage: quality
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_SOURCE_DIR: .
    SONAR_ANALYSIS_ARGS: >-
      -Dsonar.host.url=${SONAR_HOST_URL}
      -Dsonar.organization=sophie-pan
      -Dsonar.projectKey=sophie-pan_ynov-pipeline
      -Dsonar.projectName=ynov-pipeline
      -Dsonar.projectBaseDir=${CI_PROJECT_DIR}
      -Dsonar.sources=${SONAR_SOURCE_DIR}
      -Dsonar.exclusions=${SONAR_EXCLUSIONS}
      -Dsonar.sourceEncoding=UTF-8
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  script:
    - sonar-scanner $SONAR_ANALYSIS_ARGS
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  rules:
    - if: $CI_COMMIT_TAG != null || $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"

build_image:
  image: docker
  services:
    - docker:dind
  stage: package
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  rules:
    - if: $CI_COMMIT_TAG != null || $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "main"

trivy_container_scanning:
  stage: security
  image:
    name: alpine:3.11
  variables:
    GIT_STRATEGY: fetch
  allow_failure: true
  before_script:
    - export TRIVY_VERSION=${TRIVY_VERSION:-v0.19.2}
    - apk add --no-cache curl docker-cli
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin ${TRIVY_VERSION}
    - curl -sSL -o /tmp/trivy-gitlab.tpl https://github.com/aquasecurity/trivy/raw/${TRIVY_VERSION}/contrib/gitlab.tpl
  script:
    - trivy --exit-code 0 --no-progress $IMAGE_TAG
  only:
    - merge_requests
    - main
    - tags

deploy_docker:
  stage: deploy
  image: docker:stable
  variables:
    PLAY_WITH_DOCKER: ip172-18-0-98-ch13k3ae69v000fvidig
  script:
    - apk add --no-cache python3-dev python3 openssh-client py-pip libffi-dev openssl-dev gcc libc-dev make
    - pip install --upgrade pip
    - pip install docker-compose
    - export DOCKER_HOST=tcp://$PLAY_WITH_DOCKER.direct.labs.play-with-docker.com:2375
    - docker-compose down
    - docker-compose pull
    - docker-compose up -d
  environment:
    name: staging
    url: http://$PLAY_WITH_DOCKER.direct.labs.play-with-docker.com
  rules:
    - if: $BYPASS_BUILD == "true"

.deploy_aws:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  variables:
    ECS_SERVICE_NAME: "ynov-pipeline-service"
  script:
    - aws ecs update-service --cluster $ECS_CLUSTER_NAME --service $ECS_SERVICE_NAME --desired-count 1 --force-new-deployment

deploy_tmp:
  extends: .deploy_aws
  variables:
    ECS_CLUSTER_NAME: "tmp"
  only:
    - merge_requests

deploy_staging:
  extends: .deploy_aws
  variables:
    ECS_CLUSTER_NAME: "staging"
  only:
    - main

deploy_prod:
  extends: .deploy_aws
  variables:
    ECS_CLUSTER_NAME: "production"
  rules:
    - if: $CI_COMMIT_TAG != null

.robotframework:
  image: registry.gitlab.com/docker42/rfind:master
  stage: acceptance
  variables:
    ROBOT_TESTS_DIR: "robotframework/"
    ROBOT_CLI: "run-tests-in-virtual-screen"
  script:
    - $ROBOT_CLI $PABOT_OPT $ROBOT_OPT -v BROWSER:$BROWSER $ROBOT_TESTS_DIR
  artifacts:
    paths:
      - output.xml
      - log.html
      - report.html
  only:
    - main
    - merge_requests

deploy-gke:
  stage: deploy
  image: google/cloud-sdk
  script:
    - echo "$SERVICE_ACCOUNT_KEY" > key.json
    - gcloud auth activate-service-account --key-file=key.json
    - gcloud config set project high-mountain-385709
    - gcloud container clusters get-credentials cluster-1 --zone europe-west3-a --project high-mountain-385709
    - kubectl apply -f deployment.yml
  only:
    - main

robotframework:chrome:
  extends: .robotframework
  variables:
    BROWSER: chrome

robotframework:firefox:
  extends: .robotframework
  variables:
    BROWSER: firefox

tdv:
  stage: tdv
  rules:
    - if: $CI_COMMIT_TAG != null
  script:
    - |
      if [ $? -eq 0 ] && [ $(curl -o /dev/null -s -w "%{http_code}\n" http://ec2co-ecsel-19ftpr9mqrlfa-1091891056.us-west-2.elb.amazonaws.com/) -eq 200 ]; then
        echo "OK"
      else
        echo "KO"
        exit 1
      fi

generate_release:
  stage: release
  script:
    - LAST_TAG=$(git tag -l --sort=-v:refname | head -1)
    - PREVIOUS_TAG=$(git tag -l --sort=-v:refname | head -2 | tail -1)
    - COMMITS=$(git log $PREVIOUS_TAG..$LAST_TAG --pretty=format:"%s")
    - |
      curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $CUSTOM_PRIVATE_TOKEN" \
      --data '{ "name": "New release", "tag_name": "'$CI_COMMIT_TAG'", "description": "'"$COMMITS"'" }' \
      --request POST "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases"
  only:
    - tags
